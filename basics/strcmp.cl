class Main inherits A2I {

    let s1: String <- "cool\0";

    main() : Object {
        let out: String <- "";
        let s2: String <- (new IO).in_string().concat("\0");
        let i: Int <- 0 in {
            while ((not (s1[i] = 0)) && (not (s2[i] = 0))) loop {
                if (not (s1[i] - s2[i] = 0)) then
                    let out: String <- "not ";
                fi;
                i <- i + 1;
            } pool;
            out;
        }
        (new IO).out_string(out + "equal");
    };

};
