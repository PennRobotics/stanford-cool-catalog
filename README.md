Stanford Cool Catalog
=====================

## Ideas

- [ ] basics
    - [x] hello world
    - [ ] strcmp
    - [ ] merge sort
    - [ ] concat arrays
    - [ ] factorial
    - [ ] prime sieve
- [ ] games
    - [ ] guess a number
    - [ ] Penney's game
    - [ ] rock, paper, scissors
    - [ ] hangman
    - [ ] tic tac toe
    - [ ] battleship
    - [ ] 2048
- [ ] calculation
    - [ ] Spigot algorithm
    - [ ] trigonometry library
    - [ ] Heron's formula
    - [ ] surface areas of various solids
    - [ ] PRNG (e.g. Park-Miller)
    - [ ] Gauss-Seidel solver
    - [ ] bspline
    - [ ] date/time utils
- [ ] music
    - [ ] reduced ABC to RAW
    - [ ] RAW to WAV
    - [ ] 99 bottles of beer song lyrics
- [ ] etc
    - [ ] morse translator
    - [ ] ANSI code stripper
    - [ ] roman numeral conversion
